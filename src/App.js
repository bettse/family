import React, {useState} from 'react';

import {When} from 'react-if';

import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';

import 'emoji-mart/css/emoji-mart.css';
import {Emoji as EMEmoji} from 'emoji-mart';
import Emoji from 'a11y-react-emoji';

import ClickToCopy from 'react-copy-content';

import useLocalStorage from './useLocalStorage';
import Footer from './Footer';
import Header from './Header';

import './App.css';

const ZWJ = '‍';
const skins = [1, 2, 3, 4, 5, 6];
const family_components = ['man', 'woman', 'girl', 'boy'];
const MAX_PARTS = 100;

function App() {
  const [parts, setParts] = useState([]);
  const [showInspiration, setShowInspiration] = useLocalStorage(
    'showInspiration',
    true,
  );
  const [showHowto, setShowHowto] = useLocalStorage('showHowto', true);

  function updateFamily(emoji, event) {
    if (parts.length < MAX_PARTS) {
      setParts([...parts, emoji.native]);
    }
  }

  function skinVariations(emoji) {
    return skins.map(skin => {
      return (
        <EMEmoji
          emoji={emoji}
          key={skin}
          size={64}
          skin={skin}
          set="apple"
          native
          onClick={updateFamily}
        />
      );
    });
  }

  function reset() {
    setParts([]);
  }

  const family = parts.join(ZWJ);
  const familyFontSize = Math.min(128, 128 / Math.ceil(parts.length / 8));
  return (
    <>
      <Header />
      <Container fluid="md">
        <Row>
          <Col xs={12} sm={6}>
            <Accordion activeKey={showInspiration}>
              <Card>
                <Accordion.Toggle
                  as={Card.Header}
                  eventKey={true}
                  onClick={() => setShowInspiration(!showInspiration)}>
                  Inspiration (click to {showInspiration ? 'hide' : 'show'})
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={true}>
                  <Card.Body>
                    I was looking for an emoji for part of my family where not
                    everyone shares the same skin tone. This lead me to the{' '}
                    <a href="https://emojipedia.org/families/">
                      Emojipedia page on families
                    </a>{' '}
                    and I realized that there are a lot of combinations that
                    don't have a single emoji. It took me some effort to even
                    produce the ZWJ-based sequence relevent to my need, so I
                    thought the internet might find a tool to make this easier
                    useful. It can also serve to test if there is a single emoji
                    for those combinations.
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </Col>
          <Col xs={12} sm={6}>
            <Accordion activeKey={showHowto}>
              <Card>
                <Accordion.Toggle
                  as={Card.Header}
                  eventKey={true}
                  onClick={() => setShowHowto(!showHowto)}>
                  How to (click to {showHowto ? 'hide' : 'show'})
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={true}>
                  <Card.Body>
                    An easy sequence for producing a single emoji (👨‍👩‍👧‍👦) is to
                    click the first person in each group [👨,👩,👧,👦]. There
                    are also a number of single emoji for single adult or
                    same-sex couples with one or two children: 👩‍👩‍👦, 👨‍👧‍👧.
                    Unfortunately, the combination I was trying, 👨🏾‍👩🏻‍👧🏽, is
                    still not handled as a single emoji. Maybe someday.
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </Col>
        </Row>
        <Row className="text-center justify-content-center mt-5">
          <Col>
            To produce your own sequence: Select one or two Adult(s), then one
            or two children
          </Col>
        </Row>
        <Row className="text-center justify-content-center">
          {family_components.map(component => {
            return (
              <Col key={component} xs={6} sm={3}>
                {skinVariations(component)}
              </Col>
            );
          })}
        </Row>

        <Row className="text-center justify-content-center">
          <Col xs={6} sm={12}>
            <ClickToCopy
              contentToCopy="This will be copied to the clipboard"
              render={props => (
                <Emoji
                  symbol={family}
                  label="custom family emoji"
                  style={{fontSize: `${familyFontSize}px`}}
                  onClick={props.copy}
                />
              )}
            />
          </Col>
        </Row>
        <When condition={parts.length > 0}>
          <Row className="text-center justify-content-center">
            <Col>
              <ClickToCopy contentToCopy={family} />
            </Col>
          </Row>
          <Row className="text-center justify-content-center mt-1">
            <Col>
              <Button onClick={reset}>Reset</Button>
            </Col>
          </Row>
        </When>
      </Container>
      <Footer />
    </>
  );
}
export default App;
