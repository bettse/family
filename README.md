# Family

[![Netlify Status](https://api.netlify.com/api/v1/badges/1a2c3dd7-387b-49e0-a30e-b38e4c090770/deploy-status)](https://app.netlify.com/sites/familyemojimaker/deploys)

This allows you to select the members of your family and copy the unicode for the emoji. If the platform/browser/etc you are on supports a custom emoji for that combination, it will be rendered.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
